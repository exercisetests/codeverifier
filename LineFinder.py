import sys, getopt
import os

from colorama import init as coloramaInit
from termcolor import colored
 
coloramaInit()
 
def readFileToString(filename):
    
    # open the sample file used
    file = open(filename)
    
    # read the content of the file opened
    content = file.readlines()

    return content

def removeTabs(fileContent):
    contentWithoutTabs = ""
    for line in fileContent:
        line.replace('\t', '')
        contentWithoutTabs += line
    return contentWithoutTabs

def findLineInFile(fileName, LineToFind):
    fileContent = readFileToString(fileName)
    cleanContent = removeTabs(fileContent)
    searchResult = sys.argv[1] in cleanContent

    return searchResult

def getAllCPPFileNamesInLocalDir():
    # assign directory
    directory = "C:\\Users\\tomer\\Desktop\\TestMagshimim\\codeverifier"
    files = []

    # iterate over files in
    # that directory
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        # checking if it is a file
        if os.path.isfile(f):
            if(filename.endswith('.h') or filename.endswith('.cpp')):
                files.append(filename)
    return files

def main(argv, checkAll = False):

    lineToCheck = argv[1]
    filesToCheck = argv[2:]

    print(str(lineToCheck) + "\n")

    if(checkAll):
        print(colored('checking all files\n', 'cyan'))
        filesToCheck = getAllCPPFileNamesInLocalDir()
        print(colored(str(filesToCheck) + "\n", 'cyan'))
    else:
        print(colored('checking the following files:\n', 'cyan'))
        print(colored(str(filesToCheck) + "\n", 'cyan'))

    searchResult = False
    for fileName in filesToCheck:
        searchResult = findLineInFile(fileName, lineToCheck)
        print(str(fileName) + " --- " + str(searchResult))
        if(searchResult):
            print(colored('Found \"' + str(lineToCheck) + '\" in file \"' + str(fileName) + '\"\n', 'green'))
            return 0

    print(colored('Couldn\'t find the line in file\n', 'yellow'))
    return 1

if __name__ == "__main__":
    if len(sys.argv) > 2:
        if(sys.argv[2] == '--all' or sys.argv[2] == '-a'):
            main(sys.argv, True)
        else:
            main(sys.argv)
    else:
        print(colored('Wrong number of arguments\n', 'red'))
        print(colored('please provide 2 arguments:\n[1] line to look for\n[2] the files to look in (--all or -a to look in all files)\n', 'yellow'))
        print(colored('example1: python LineFinder "using namespace std;" "Vector.h" "Vector.cpp"\n', 'cyan'))
        print(colored('example2: python LineFinder "using namespace std;" --all\n', 'cyan'))
        print(colored('example3: python LineFinder "using namespace std;" -a\n', 'cyan'))
