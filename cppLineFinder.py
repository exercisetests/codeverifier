#!/usr/bin/env python3
import os
import sys
import subprocess
import json
from colorama import init as coloramaInit
from termcolor import colored

coloramaInit()

# Reads the error messages from the 'rules.txt' file in the format:
# name1 @@ message1
# name2 @@ message2
# nameN @@ messageN

def readMessagesFromFile(messagesFileName):
    checkToMessageMapping = {}
    script_dir = os.path.dirname(os.path.abspath(__file__))
    fileLocation = os.path.join(script_dir, messagesFileName)
    with open(fileLocation, 'r') as file:
        for line in file:
            if "@@" in line:
                name, message = line.strip().split(" @@ ", 1)
                checkToMessageMapping[name.strip()] = message.strip()

    return checkToMessageMapping


def grep_search(directory, search_string, grep_flags=[]):
    results = []
    messages = readMessagesFromFile("rules.txt")
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".cpp"):
                file_path = os.path.join(root, file)
                try:
                    cmd = ['grep'] + grep_flags + [search_string, file_path]
                    result = subprocess.run(cmd, check=True, capture_output=True, text=True)
                    if result.returncode == 0:
                        output = result.stdout.strip().split("\n")
                        for line in output:
                            line_number = line.split(':')[0]
                            record = {
                                "name": search_string,
                                "category": "coding",
                                "message": messages[search_string],
                                "cout": f"Found '{search_string}' in file {file_path} line {line_number}"
                            }
                            results.append(record)
                except subprocess.CalledProcessError:
                    # grep did not find the search string in the file, so we continue to the next file
                    continue
    return results

def exportResultsToJson(jsonFileName, grepResults):
    # Save to JSON file
    script_dir = os.path.dirname(os.path.abspath(__file__))
    fileLocation = os.path.join(script_dir, jsonFileName)

    with open(fileLocation, "w") as outfile:
        json.dump({"jobs": grepResults}, outfile, indent=4)
    print("Results saved to " + jsonFileName)

if __name__ == "__main__":

    print("argumrents: ", sys.argv)

    grepFlags = ['-n'] # flags are hard-coded
    args = sys.argv[1:]

    if len(args) != 2:
        print(colored('Wrong number of arguments\n', 'red'))
        print(colored('please provide 2 arguments:\n[1] directory\n[2] line to look for\n', 'yellow'))
        print(colored('Usage: cppLineFinder <directory> <line to find in double quotes>\n', 'magenta'))
        print(colored('example1: ./cppLineFinder.py ./studentExercisesDirectory "printf"\n', 'cyan'))
        print(colored('example2: ./cppLineFinder.py ./studentExercisesDirectory "using namespace std;"\n', 'cyan'))
        print(colored('Wrong number of arguments\n', 'red'))
        print(colored('please provide 2 arguments:\n[1] directory\n[2] line to look for\n', 'yellow'))
        print(colored('Usage: cppLineFinder <directory> <line to find in double quotes>\n', 'magenta'))
        print(colored('example1: ./cppLineFinder.py ./studentExercisesDirectory "printf"\n', 'cyan'))
        print(colored('example2: ./cppLineFinder.py ./studentExercisesDirectory "using namespace std;"\n', 'cyan'))
        sys.exit(1)

    directory = args[0]
    results = []
    if(sys.argv[2] == '--all' or sys.argv[2] == '-a'):
        stringsToSearch = list(readMessagesFromFile("rules.txt"))
        for searchString in stringsToSearch:
            results += grep_search(directory, searchString, grepFlags)
    else:
        searchString = args[1]
        print("looking for \'" + searchString + "\'...\n")
        results = grep_search(directory, searchString, grepFlags)

    exportResultsToJson("result.json", results)

