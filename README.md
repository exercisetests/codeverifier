# CodeVerifier

## CPP Line Finder


### General
A wrapper for grep that is used to locate patterns inside cpp code.<br>
cppLineFinder looks in ALL of the files in the directory that ends with '.cpp'. <br>
It lists the line number and the line that the pattern was found in, and exports the result to json file.

### Requirements
termcolor & colorama<br>
to install: pip3 install termcolor

### Usage
cppLineFinder <*flags directory*> <*line to find in double quotes*>

### Example

./cppLineFinder.py ./StudentExercisesDirectory "using namespace std;"

