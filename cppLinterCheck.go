package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const (
	RULES_JSON       = "cppCheckResults.json"
	CENTRALIZE_JSONS = "centralize"
)

type Rule struct {
	Category string   `json:"category"`
	Name     string   `json:"name"`
	Message  string   `json:"message"`
	Id       []string `json:"id"`
	File     string   `json:"file"`
	Line     string   `json:"line"`
}

type RulesJson map[string]Rule

type Location struct {
	File   string `xml:"file,attr"`
	Line   int    `xml:"line,attr"`
	Column int    `xml:"column,attr"`
	Info   string `xml:"info,attr,omitempty"`
}

type XmlErrorEntity struct {
	Id       string   `xml:"id,attr"`
	Msg      string   `xml:"msg,attr"`
	Verbose  string   `xml:"verbose,attr"`
	Cwe      string   `xml:"cwe,attr,omitempty"`
	Location Location `xml:"location"`
}

type Errors struct {
	Errors []XmlErrorEntity `xml:"error"`
}

type Results struct {
	Errors Errors `xml:"errors"`
}

func main() {

	if len(os.Args) < 2 {
		fmt.Println("Usage: go run cppLinterCheck.go <directory_path>")
		return
	}

	dirPath := os.Args[1]

	if dirPath == CENTRALIZE_JSONS {
		centralizeJsons()
	} else {
		extractAllRules(dirPath)
	}
}

func extractAllRules(dirPath string) {
	if _, err := os.Stat(dirPath); os.IsNotExist(err) {
		fmt.Printf("Error: Directory [%v] does not exist.\n", dirPath)
		return
	}

	if !isDir(dirPath) {
		fmt.Printf("Error: The provided path [%v] is not a directory.\n", dirPath)
		return
	}

	rulesJson := RulesJson{}
	rulesJsonPath := filepath.Join(dirPath, RULES_JSON)
	if _, err := os.Stat(rulesJsonPath); !os.IsNotExist(err) {
		data, err := os.ReadFile(rulesJsonPath)
		if err != nil {
			fmt.Printf("Error reading the file [%v] : %v \n", rulesJsonPath, err.Error())
			return
		}

		err = json.Unmarshal(data, &rulesJson)
		if err != nil {
			fmt.Printf("Error unmarshaling the file [%v] : %v \n", rulesJsonPath, err.Error())
			return
		}
	}

	rulesMap := map[string]bool{}
	for _, rule := range rulesJson {
		rulesMap[rule.Message] = true
	}

	fmt.Println(rulesMap)

	fmt.Printf("--- len rules before travel: [%v]\n", len(rulesJson))
	travelDirectSubDirs(dirPath, rulesMap, rulesJson)
	fmt.Printf("--- len rules after travel: [%v]\n", len(rulesJson))

	data, err := json.MarshalIndent(rulesJson, "", "	")
	if err != nil {
		fmt.Printf("could not marshal json data, err: %v\n", err.Error())
	}

	fmt.Println("overriding cppCheckResults.json file with new content...")
	err = os.WriteFile(rulesJsonPath, data, 0644)
	if err != nil {
		fmt.Printf("could not write out file to override [%v], err: %v\n", rulesJsonPath, err.Error())
	}
	fmt.Println("done.")
}

func centralizeJsons() {

	centralizedRulesJson := RulesJson{}

	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Error getting current directory:", err)
		return
	}

	fmt.Println("reading current directory, listing all direct folders")
	dirEntries, err := os.ReadDir(cwd)
	if err != nil {
		fmt.Println("Error reading directory:", err)
		return
	}

	for _, entry := range dirEntries {
		if entry.IsDir() {
			rulesFilePath := filepath.Join(cwd, entry.Name(), RULES_JSON)
			fmt.Printf("expecting file [%v] cppCheckResults.json...\n", rulesFilePath)
			info, err := os.Stat(rulesFilePath)
			rulesJson := RulesJson{}
			if !os.IsNotExist(err) {
				data, err := os.ReadFile(rulesFilePath)
				if err != nil {
					fmt.Printf("Error reading the file [%v] : %v \n", rulesFilePath, err.Error())
					return
				}

				err = json.Unmarshal(data, &rulesJson)
				if err != nil {
					fmt.Printf("Error unmarshaling the file [%v] : %v \n", rulesFilePath, err.Error())
					return
				}
			}
			if info.IsDir() {
				continue
			}

			fmt.Println("done loading content, performing keys scan, keys that did not appear yet will be added to the centralized rules json...")
			added := 0
			for k, v := range rulesJson {
				if _, exists := centralizedRulesJson[k]; !exists {
					added++
					centralizedRulesJson[k] = v
				}
			}
			fmt.Printf("done scanning keys of file [%v] added keys [%v]\n", rulesFilePath, added)
		}
	}

	data, err := json.MarshalIndent(centralizedRulesJson, "", "	")
	if err != nil {
		fmt.Printf("could not marshal json data, err: %v\n", err.Error())
	}

	fmt.Printf("writing out centralized rules json content into file named [%v] json size [%v]...\n", RULES_JSON, len(centralizedRulesJson))
	err = os.WriteFile(RULES_JSON, data, 0644)
	if err != nil {
		fmt.Printf("could not write out file to override [%v], err: %v\n", RULES_JSON, err.Error())
	}
	fmt.Println("done.")

}

func isDir(path string) bool {
	info, err := os.Stat(path)
	if err != nil {
		return false
	}
	return info.IsDir()
}

func travelDirectSubDirs(dirPath string, rulesMap map[string]bool, rules map[string]Rule) {
	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.Contains(path, "git") {
			return nil
		}
		if info.IsDir() {
			fmt.Printf("path: [%v]\n", path)

			absPath, err := filepath.Abs(path)
			if err != nil {
				fmt.Printf("error getting absolute path for dir [%v] err: %v\n", path, err.Error())
				return err
			}

			cmdOutput, err := exec.Command("cppcheck", absPath, "--enable=all", "--force", "--xml", "2").CombinedOutput()
			if err != nil {
				fmt.Printf("error executing command 'cppcheck' on dir [%v] err: %v\n", absPath, err.Error())
				fmt.Println(string(cmdOutput))
				return err
			}

			xmlFile := string(cmdOutput)
			xmlPath := fmt.Sprintf("%v/output.xml", dirPath)

			fmt.Printf("writing out xml content of length [%v] to [%v]\n", len(xmlFile), xmlPath)
			os.WriteFile(xmlPath, cmdOutput, 0644)

			fmt.Println("unmarshaling xml content into struct entity...")
			var results Results
			err = xml.Unmarshal(cmdOutput, &results)
			if err != nil {
				fmt.Println("Error unmarshaling:", err.Error())
				return err
			}

			for _, xmlError := range results.Errors.Errors {
				if _, exists := rulesMap[xmlError.Msg]; !exists {
					rulesMap[xmlError.Msg] = true
					file := xmlError.Location.File
					splt := strings.Split(file, "\\")
					idx := len(splt) - 1
					if idx >= 0 {
						file = splt[idx]
					}
					newRule := Rule{
						Category: "coding",
						Name:     xmlError.Id,
						Message:  xmlError.Msg,
						File:     file,
						Line:     fmt.Sprint(xmlError.Location.Line),
					}
					rules[xmlError.Id] = newRule
					fmt.Printf("added to rules: [%v]\n", newRule)
				}
			}
		}
		return nil
	})

	if err != nil {
		fmt.Println("Error listing subdirectories:", err.Error())
	}
}
